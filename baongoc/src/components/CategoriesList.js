import React from 'react';
import Slider from "react-slick";

class CategoriesList extends React.Component {
	render() {
		const settings = {
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: 6,
			slidesToScroll: 1
		};
		return (
			<div className="axil-categories-list axil-section-gap bg-color-grey">
				<div className="container">
					<div className="row align-items-center mb--30">
						<div className="col-lg-6 col-md-8 col-sm-8 col-12">
							<div className="section-title">
								<h2 className="title">Trending Topics</h2>
							</div>
						</div>
						<div className="col-lg-6 col-md-4 col-sm-4 col-12">
							<div className="see-all-topics text-left text-sm-right mt_mobile--20">
								<a className="axil-link-button">See All Topics</a>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-lg-12">
							{/* Start List Wrapper  */}
							<div className="list-categories categories-activation axil-slick-arrow arrow-between-side">
								<Slider {...settings} >
									{/* Start Single Category  */}
									<div className="single-cat">
										<div className="inner">
											<a>
												<div className="thumbnail">
													<img src="assets/images/post-images/post-sm-01.jpg"
														 alt="post categories images"/>
												</div>
												<div className="content">
													<h5 className="title">Sports &amp; Fitness </h5>
												</div>
											</a>
										</div>
									</div>
									{/* End Single Category  */}
									{/* Start Single Category  */}
									<div className="single-cat">
										<div className="inner">
											<a>
												<div className="thumbnail">
													<img src="assets/images/post-images/post-sm-02.jpg"
														 alt="post categories images"/>
												</div>
												<div className="content">
													<h5 className="title">Travel</h5>
												</div>
											</a>
										</div>
									</div>
									{/* End Single Category  */}
									{/* Start Single Category  */}
									<div className="single-cat">
										<div className="inner">
											<a>
												<div className="thumbnail">
													<img src="assets/images/post-images/post-sm-03.jpg"
														 alt="post categories images"/>
												</div>
												<div className="content">
													<h5 className="title">lifestyle</h5>
												</div>
											</a>
										</div>
									</div>
									{/* End Single Category  */}
									{/* Start Single Category  */}
									<div className="single-cat">
										<div className="inner">
											<a>
												<div className="thumbnail">
													<img src="assets/images/post-images/post-sm-04.jpg"
														 alt="post categories images"/>
												</div>
												<div className="content">
													<h5 className="title">Health</h5>
												</div>
											</a>
										</div>
									</div>
									{/* End Single Category  */}
									{/* Start Single Category  */}
									<div className="single-cat">
										<div className="inner">
											<a>
												<div className="thumbnail">
													<img src="assets/images/post-images/post-sm-05.jpg"
														 alt="post categories images"/>
												</div>
												<div className="content">
													<h5 className="title">Animals</h5>
												</div>
											</a>
										</div>
									</div>
									{/* End Single Category  */}
									{/* Start Single Category  */}
									<div className="single-cat">
										<div className="inner">
											<a>
												<div className="thumbnail">
													<img src="assets/images/post-images/post-sm-06.jpg"
														 alt="post categories images"/>
												</div>
												<div className="content">
													<h5 className="title">Food &amp; Drink</h5>
												</div>
											</a>
										</div>
									</div>
									{/* End Single Category  */}
									{/* Start Single Category  */}
									<div className="single-cat">
										<div className="inner">
											<a>
												<div className="thumbnail">
													<img src="assets/images/post-images/post-sm-06.jpg"
														 alt="post categories images"/>
												</div>
												<div className="content">
													<h5 className="title">Food &amp; Drink</h5>
												</div>
											</a>
										</div>
									</div>
									{/* End Single Category  */}
									{/* Start Single Category  */}
									<div className="single-cat">
										<div className="inner">
											<a>
												<div className="thumbnail">
													<img src="assets/images/post-images/post-sm-06.jpg"
														 alt="post categories images"/>
												</div>
												<div className="content">
													<h5 className="title">Food &amp; Drink</h5>
												</div>
											</a>
										</div>
									</div>
									{/* End Single Category  */}
								</Slider>
							</div>
							{/* Start List Wrapper  */}
						</div>
					</div>
				</div>
			</div>
		);
	}

}

export default CategoriesList;
