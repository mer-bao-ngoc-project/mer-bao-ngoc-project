import React from 'react';

function PostGrid() {
    return (
        <div className="axil-post-grid-area axil-section-gap bg-color-grey">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="section-title">
                            <h2 className="title">Most Popular</h2>
                        </div>
                    </div>
                    <div className="col-lg-12">
                        <ul className="axil-tab-button nav nav-tabs mt--20" role="tablist">
                            <li className="nav-item" role="presentation">
                                <a className="nav-link active" id="grid-one" data-toggle="tab" href="#gridone" role="tab" aria-controls="grid-one" aria-selected="true">Accessibility</a>
                            </li>
                            <li className="nav-item" role="presentation">
                                <a className="nav-link" id="grid-two" data-toggle="tab" href="#gridtwo" role="tab" aria-controls="grid-two" aria-selected="false">Android Dev</a>
                            </li>
                            <li className="nav-item" role="presentation">
                                <a className="nav-link" id="grid-three" data-toggle="tab" href="#gridthree" role="tab" aria-controls="grid-three" aria-selected="false">Blockchain</a>
                            </li>
                            <li className="nav-item" role="presentation">
                                <a className="nav-link" id="grid-four" data-toggle="tab" href="#gridfour" role="tab" aria-controls="grid-four" aria-selected="false">Gadgets</a>
                            </li>
                        </ul>
                        {/* Start Tab Content  */}
                        <div className="grid-tab-content tab-content">
                            {/* Start Single Tab Content  */}
                            <div className="single-post-grid tab-pane fade show active" id="gridone" role="tabpanel">
                                <div className="row">
                                    <div className="col-xl-5 col-lg-5 col-md-12 col-12">
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-12">
                                                {/* Start Post Grid  */}
                                                <div className="content-block post-grid mt--30">
                                                    <div className="post-thumbnail">
                                                        <a >
                                                            <img src="assets/images/post-images/lifestyle-grid-01.jpg" alt="Post Images" />
                                                        </a>
                                                    </div>
                                                    <div className="post-grid-content">
                                                        <div className="post-content">
                                                            <div className="post-cat">
                                                                <div className="post-cat-list">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="LIFESTYLE">LIFESTYLE</span>
                                  </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <h4 className="title"><a >iPadOS 14 new
                                                                designed specifically for iPad</a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* Start Post Grid  */}
                                            </div>
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-12">
                                                {/* Start Post Grid  */}
                                                <div className="content-block post-grid mt--30">
                                                    <div className="post-thumbnail">
                                                        <a >
                                                            <img src="assets/images/post-images/lifestyle-grid-02.jpg" alt="Post Images" />
                                                        </a>
                                                    </div>
                                                    <div className="post-grid-content">
                                                        <div className="post-content">
                                                            <div className="post-cat">
                                                                <div className="post-cat-list">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="TRAVEL">TRAVEL</span>
                                  </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <h4 className="title"><a >Apple
                                                                reimagines the iPhone experience with iOS 14</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* Start Post Grid  */}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-7 col-lg-7 col-md-12 col-12">
                                        {/* Start Post Grid  */}
                                        <div className="content-block post-grid post-grid-large mt--30">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/post-images/lifestyle-grid-03.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-grid-content">
                                                <div className="post-content">
                                                    <div className="post-cat">
                                                        <div className="post-cat-list">
                                                            <a className="hover-flip-item-wrapper" >
                              <span className="hover-flip-item">
                                <span data-text="DESIGN">DESIGN</span>
                              </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 className="title"><a >Eating right is part
                                                        of my lifestyle now.</a></h3>
                                                    <div className="post-meta-wrapper">
                                                        <div className="post-meta">
                                                            <div className="post-author-avatar border-rounded">
                                                                <img src="assets/images/post-images/author/author-image-3.png" alt="Author Images" />
                                                            </div>
                                                            <div className="content">
                                                                <h6 className="post-author-name">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="Rahabi Khan">Rahabi Khan</span>
                                  </span>
                                                                    </a>
                                                                </h6>
                                                                <ul className="post-meta-list">
                                                                    <li>Feb 17, 2019</li>
                                                                    <li>300k Views</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <ul className="social-share-transparent justify-content-end">
                                                            <li><a ><i className="fab fa-facebook-f" /></a></li>
                                                            <li><a ><i className="fab fa-instagram" /></a></li>
                                                            <li><a ><i className="fab fa-twitter" /></a></li>
                                                            <li><a ><i className="fas fa-link" /></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* Start Post Grid  */}
                                    </div>
                                </div>
                            </div>
                            {/* End Single Tab Content  */}
                            {/* Start Single Tab Content  */}
                            <div className="single-post-grid tab-pane fade" id="gridtwo" role="tabpanel">
                                <div className="row">
                                    <div className="col-xl-5 col-lg-5 col-md-12 col-12">
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-12">
                                                {/* Start Post Grid  */}
                                                <div className="content-block post-grid mt--30">
                                                    <div className="post-thumbnail">
                                                        <a >
                                                            <img src="assets/images/post-images/lifestyle-grid-01.jpg" alt="Post Images" />
                                                        </a>
                                                    </div>
                                                    <div className="post-grid-content">
                                                        <div className="post-content">
                                                            <div className="post-cat">
                                                                <div className="post-cat-list">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="LIFESTYLE">LIFESTYLE</span>
                                  </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <h4 className="title"><a >iPadOS 14 new
                                                                designed specifically for iPad</a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* Start Post Grid  */}
                                            </div>
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-12">
                                                {/* Start Post Grid  */}
                                                <div className="content-block post-grid mt--30">
                                                    <div className="post-thumbnail">
                                                        <a >
                                                            <img src="assets/images/post-images/lifestyle-grid-02.jpg" alt="Post Images" />
                                                        </a>
                                                    </div>
                                                    <div className="post-grid-content">
                                                        <div className="post-content">
                                                            <div className="post-cat">
                                                                <div className="post-cat-list">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="TRAVEL">TRAVEL</span>
                                  </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <h4 className="title"><a >Apple
                                                                reimagines the iPhone experience with iOS 14</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* Start Post Grid  */}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-7 col-lg-7 col-md-12 col-12">
                                        {/* Start Post Grid  */}
                                        <div className="content-block post-grid post-grid-large mt--30">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/post-images/lifestyle-grid-03.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-grid-content">
                                                <div className="post-content">
                                                    <div className="post-cat">
                                                        <div className="post-cat-list">
                                                            <a className="hover-flip-item-wrapper" >
                              <span className="hover-flip-item">
                                <span data-text="DESIGN">DESIGN</span>
                              </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 className="title"><a >Eating right is part
                                                        of my lifestyle now.</a></h3>
                                                    <div className="post-meta-wrapper">
                                                        <div className="post-meta">
                                                            <div className="post-author-avatar border-rounded">
                                                                <img src="assets/images/post-images/author/author-image-3.png" alt="Author Images" />
                                                            </div>
                                                            <div className="content">
                                                                <h6 className="post-author-name">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="Rahabi Khan">Rahabi Khan</span>
                                  </span>
                                                                    </a>
                                                                </h6>
                                                                <ul className="post-meta-list">
                                                                    <li>Feb 17, 2019</li>
                                                                    <li>300k Views</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <ul className="social-share-transparent justify-content-end">
                                                            <li><a ><i className="fab fa-facebook-f" /></a></li>
                                                            <li><a ><i className="fab fa-instagram" /></a></li>
                                                            <li><a ><i className="fab fa-twitter" /></a></li>
                                                            <li><a ><i className="fas fa-link" /></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* Start Post Grid  */}
                                    </div>
                                </div>
                            </div>
                            {/* End Single Tab Content  */}
                            {/* Start Single Tab Content  */}
                            <div className="single-post-grid tab-pane fade" id="gridthree" role="tabpanel">
                                <div className="row">
                                    <div className="col-xl-5 col-lg-5 col-md-12 col-12">
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-12">
                                                {/* Start Post Grid  */}
                                                <div className="content-block post-grid mt--30">
                                                    <div className="post-thumbnail">
                                                        <a >
                                                            <img src="assets/images/post-images/lifestyle-grid-01.jpg" alt="Post Images" />
                                                        </a>
                                                    </div>
                                                    <div className="post-grid-content">
                                                        <div className="post-content">
                                                            <div className="post-cat">
                                                                <div className="post-cat-list">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="LIFESTYLE">LIFESTYLE</span>
                                  </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <h4 className="title"><a >iPadOS 14 new
                                                                designed specifically for iPad</a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* Start Post Grid  */}
                                            </div>
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-12">
                                                {/* Start Post Grid  */}
                                                <div className="content-block post-grid mt--30">
                                                    <div className="post-thumbnail">
                                                        <a >
                                                            <img src="assets/images/post-images/lifestyle-grid-02.jpg" alt="Post Images" />
                                                        </a>
                                                    </div>
                                                    <div className="post-grid-content">
                                                        <div className="post-content">
                                                            <div className="post-cat">
                                                                <div className="post-cat-list">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="TRAVEL">TRAVEL</span>
                                  </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <h4 className="title"><a >Apple
                                                                reimagines the iPhone experience with iOS 14</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* Start Post Grid  */}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-7 col-lg-7 col-md-12 col-12">
                                        {/* Start Post Grid  */}
                                        <div className="content-block post-grid post-grid-large mt--30">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/post-images/lifestyle-grid-03.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-grid-content">
                                                <div className="post-content">
                                                    <div className="post-cat">
                                                        <div className="post-cat-list">
                                                            <a className="hover-flip-item-wrapper" >
                              <span className="hover-flip-item">
                                <span data-text="DESIGN">DESIGN</span>
                              </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 className="title"><a >Eating right is part
                                                        of my lifestyle now.</a></h3>
                                                    <div className="post-meta-wrapper">
                                                        <div className="post-meta">
                                                            <div className="post-author-avatar border-rounded">
                                                                <img src="assets/images/post-images/author/author-image-3.png" alt="Author Images" />
                                                            </div>
                                                            <div className="content">
                                                                <h6 className="post-author-name">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="Rahabi Khan">Rahabi Khan</span>
                                  </span>
                                                                    </a>
                                                                </h6>
                                                                <ul className="post-meta-list">
                                                                    <li>Feb 17, 2019</li>
                                                                    <li>300k Views</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <ul className="social-share-transparent justify-content-end">
                                                            <li><a ><i className="fab fa-facebook-f" /></a></li>
                                                            <li><a ><i className="fab fa-instagram" /></a></li>
                                                            <li><a ><i className="fab fa-twitter" /></a></li>
                                                            <li><a ><i className="fas fa-link" /></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* Start Post Grid  */}
                                    </div>
                                </div>
                            </div>
                            {/* End Single Tab Content  */}
                            {/* Start Single Tab Content  */}
                            <div className="single-post-grid tab-pane fade" id="gridfour" role="tabpanel">
                                <div className="row">
                                    <div className="col-xl-5 col-lg-5 col-md-12 col-12">
                                        <div className="row">
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-12">
                                                {/* Start Post Grid  */}
                                                <div className="content-block post-grid mt--30">
                                                    <div className="post-thumbnail">
                                                        <a >
                                                            <img src="assets/images/post-images/lifestyle-grid-01.jpg" alt="Post Images" />
                                                        </a>
                                                    </div>
                                                    <div className="post-grid-content">
                                                        <div className="post-content">
                                                            <div className="post-cat">
                                                                <div className="post-cat-list">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="LIFESTYLE">LIFESTYLE</span>
                                  </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <h4 className="title"><a >iPadOS 14 new
                                                                designed specifically for iPad</a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* Start Post Grid  */}
                                            </div>
                                            <div className="col-xl-12 col-lg-12 col-md-12 col-12">
                                                {/* Start Post Grid  */}
                                                <div className="content-block post-grid mt--30">
                                                    <div className="post-thumbnail">
                                                        <a >
                                                            <img src="assets/images/post-images/lifestyle-grid-02.jpg" alt="Post Images" />
                                                        </a>
                                                    </div>
                                                    <div className="post-grid-content">
                                                        <div className="post-content">
                                                            <div className="post-cat">
                                                                <div className="post-cat-list">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="TRAVEL">TRAVEL</span>
                                  </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <h4 className="title"><a >Apple
                                                                reimagines the iPhone experience with iOS 14</a>
                                                            </h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                {/* Start Post Grid  */}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-xl-7 col-lg-7 col-md-12 col-12">
                                        {/* Start Post Grid  */}
                                        <div className="content-block post-grid post-grid-large mt--30">
                                            <div className="post-thumbnail">
                                                <a >
                                                    <img src="assets/images/post-images/lifestyle-grid-03.jpg" alt="Post Images" />
                                                </a>
                                            </div>
                                            <div className="post-grid-content">
                                                <div className="post-content">
                                                    <div className="post-cat">
                                                        <div className="post-cat-list">
                                                            <a className="hover-flip-item-wrapper" >
                              <span className="hover-flip-item">
                                <span data-text="DESIGN">DESIGN</span>
                              </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <h3 className="title"><a >Eating right is part
                                                        of my lifestyle now.</a></h3>
                                                    <div className="post-meta-wrapper">
                                                        <div className="post-meta">
                                                            <div className="post-author-avatar border-rounded">
                                                                <img src="assets/images/post-images/author/author-image-3.png" alt="Author Images" />
                                                            </div>
                                                            <div className="content">
                                                                <h6 className="post-author-name">
                                                                    <a className="hover-flip-item-wrapper" >
                                  <span className="hover-flip-item">
                                    <span data-text="Rahabi Khan">Rahabi Khan</span>
                                  </span>
                                                                    </a>
                                                                </h6>
                                                                <ul className="post-meta-list">
                                                                    <li>Feb 17, 2019</li>
                                                                    <li>300k Views</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <ul className="social-share-transparent justify-content-end">
                                                            <li><a ><i className="fab fa-facebook-f" /></a></li>
                                                            <li><a ><i className="fab fa-instagram" /></a></li>
                                                            <li><a ><i className="fab fa-twitter" /></a></li>
                                                            <li><a ><i className="fas fa-link" /></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {/* Start Post Grid  */}
                                    </div>
                                </div>
                            </div>
                            {/* End Single Tab Content  */}
                        </div>
                        {/* End Tab Content  */}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PostGrid;
