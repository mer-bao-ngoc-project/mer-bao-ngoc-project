import React from 'react';

function FooterTop() {
    return (
        <div className="footer-top">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        {/* Start Post List  */}
                        <div className="inner d-flex align-items-center flex-wrap">
                            <h5 className="follow-title mb--0 mr--20">Follow Us</h5>
                            <ul className="social-icon color-tertiary md-size justify-content-start">
                                <li><a ><i className="fab fa-facebook-f" /></a></li>
                                <li><a ><i className="fab fa-instagram" /></a></li>
                                <li><a ><i className="fab fa-twitter" /></a></li>
                                <li><a ><i className="fab fa-linkedin-in" /></a></li>
                            </ul>
                        </div>
                        {/* End Post List  */}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FooterTop;
