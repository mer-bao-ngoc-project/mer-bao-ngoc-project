import React from 'react';

function Social() {
    return (
        <div className="row pb--70">
            <div className="col-lg-12">
                <div className="axil-social-wrapper bg-color-white radius">
                    <ul className="social-with-text">
                        <li className="twitter"><a ><i className="fab fa-twitter" /><span>Twitter</span></a></li>
                        <li className="facebook"><a ><i className="fab fa-facebook-f" /><span>Facebook</span></a></li>
                        <li className="instagram"><a ><i className="fab fa-instagram" /><span>Instagram</span></a></li>
                        <li className="youtube"><a ><i className="fab fa-youtube" /><span>Youtube</span></a></li>
                        <li className="pinterest"><a ><i className="fab fa-pinterest" /><span>Pinterest</span></a></li>
                        <li className="discord"><a ><i className="fab fa-discord" /><span>Discord</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default Social;
