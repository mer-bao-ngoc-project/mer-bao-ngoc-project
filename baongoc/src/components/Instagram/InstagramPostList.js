import React from 'react';

function InstagramPostList() {
    return (
        <div className="row mt--30">
            <div className="col-lg-12">
                <ul className="instagram-post-list">
                    <li className="single-post">
                        <a >
                            <img src="assets/images/small-images/instagram-md-06.jpg" alt="Instagram Images" />
                            <span className="instagram-button"><i className="fab fa-instagram" /></span>
                        </a>
                    </li>
                    <li className="single-post">
                        <a >
                            <img src="assets/images/small-images/instagram-md-07.jpg" alt="Instagram Images" />
                            <span className="instagram-button"><i className="fab fa-instagram" /></span>
                        </a>
                    </li>
                    <li className="single-post">
                        <a >
                            <img src="assets/images/small-images/instagram-md-08.jpg" alt="Instagram Images" />
                            <span className="instagram-button"><i className="fab fa-instagram" /></span>
                        </a>
                    </li>
                    <li className="single-post">
                        <a >
                            <img src="assets/images/small-images/instagram-md-09.jpg" alt="Instagram Images" />
                            <span className="instagram-button"><i className="fab fa-instagram" /></span>
                        </a>
                    </li>
                    <li className="single-post">
                        <a >
                            <img src="assets/images/small-images/instagram-md-10.jpg" alt="Instagram Images" />
                            <span className="instagram-button"><i className="fab fa-instagram" /></span>
                        </a>
                    </li>
                    <li className="single-post">
                        <a >
                            <img src="assets/images/small-images/instagram-md-11.jpg" alt="Instagram Images" />
                            <span className="instagram-button"><i className="fab fa-instagram" /></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
}

export default InstagramPostList;
