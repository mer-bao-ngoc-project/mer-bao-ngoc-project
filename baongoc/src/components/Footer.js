import React from 'react';
import FooterTop from './Footer/FooterTop'
import Copyright from './Footer/Copyright'
class Footer extends React.Component {
	render() {
		const dataCopyright = this.props.dataCopyright;
		return (
			<div className="axil-footer-area axil-footer-style-1 bg-color-white">

				{/* Start Footer Top Area  */}
				<FooterTop/>
				{/* End Footer Top Area  */}

				{/* Start Copyright Area  */}
				<Copyright/>
				{/* End Copyright Area  */}
			</div>
		);
	}

}

export default Footer;
