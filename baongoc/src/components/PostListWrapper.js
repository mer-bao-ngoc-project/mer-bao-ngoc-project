import React from 'react';

function PostListWrapper() {
    return (
        <div className="axil-post-list-area post-listview-visible-color axil-section-gap bg-color-white">
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 col-xl-8">
                        {/* Start Post List  */}
                        <div className="content-block post-list-view is-active mt--30">
                            <div className="post-thumbnail">
                                <a >
                                    <img src="assets/images/post-images/post-list-05.jpg" alt="Post Images" />
                                </a>
                            </div>
                            <div className="post-content">
                                <div className="post-cat">
                                    <div className="post-cat-list">
                                        <a className="hover-flip-item-wrapper" >
                    <span className="hover-flip-item">
                      <span data-text="FOOD">FOOD</span>
                    </span>
                                        </a>
                                    </div>
                                </div>
                                <h4 className="title"><a >Security isn’t just a technology problem
                                    it’s about design, too </a></h4>
                                <div className="post-meta-wrapper">
                                    <div className="post-meta">
                                        <div className="post-author-avatar border-rounded">
                                            <img src="assets/images/post-images/author/author-image-2.png" alt="Author Images" />
                                        </div>
                                        <div className="content">
                                            <h6 className="post-author-name">
                                                <a className="hover-flip-item-wrapper" >
                        <span className="hover-flip-item">
                          <span data-text="Fatima Asrafy">Fatima Asrafy</span>
                        </span>
                                                </a>
                                            </h6>
                                            <ul className="post-meta-list">
                                                <li>Feb 17, 2019</li>
                                                <li>3 min read</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <ul className="social-share-transparent justify-content-end">
                                        <li><a ><i className="fab fa-facebook-f" /></a></li>
                                        <li><a ><i className="fab fa-instagram" /></a></li>
                                        <li><a ><i className="fab fa-twitter" /></a></li>
                                        <li><a ><i className="fas fa-link" /></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {/* End Post List  */}
                        {/* Start Post List  */}
                        <div className="content-block post-list-view axil-control mt--30">
                            <div className="post-thumbnail">
                                <a >
                                    <img src="assets/images/post-images/post-list-06.jpg" alt="Post Images" />
                                </a>
                            </div>
                            <div className="post-content">
                                <div className="post-cat">
                                    <div className="post-cat-list">
                                        <a className="hover-flip-item-wrapper" >
                    <span className="hover-flip-item">
                      <span data-text="TRAVEL">TRAVEL</span>
                    </span>
                                        </a>
                                    </div>
                                </div>
                                <h4 className="title"><a >To the lover of wilderness, Alaska is one of the most wonderful. </a></h4>
                                <div className="post-meta-wrapper">
                                    <div className="post-meta">
                                        <div className="post-author-avatar border-rounded">
                                            <img src="assets/images/post-images/author/author-image-3.png" alt="Author Images" />
                                        </div>
                                        <div className="content">
                                            <h6 className="post-author-name">
                                                <a className="hover-flip-item-wrapper" >
                        <span className="hover-flip-item">
                          <span data-text="Sarah">Sarah</span>
                        </span>
                                                </a>
                                            </h6>
                                            <ul className="post-meta-list">
                                                <li>Feb 17, 2019</li>
                                                <li>3 min read</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <ul className="social-share-transparent justify-content-end">
                                        <li><a ><i className="fab fa-facebook-f" /></a></li>
                                        <li><a ><i className="fab fa-instagram" /></a></li>
                                        <li><a ><i className="fab fa-twitter" /></a></li>
                                        <li><a ><i className="fas fa-link" /></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {/* End Post List  */}
                        {/* Start Post List  */}
                        <div className="content-block post-list-view axil-control mt--30">
                            <div className="post-thumbnail">
                                <a >
                                    <img src="assets/images/post-images/post-list-07.jpg" alt="Post Images" />
                                </a>
                            </div>
                            <div className="post-content">
                                <div className="post-cat">
                                    <div className="post-cat-list">
                                        <a className="hover-flip-item-wrapper" >
                    <span className="hover-flip-item">
                      <span data-text="DESIGN">DESIGN</span>
                    </span>
                                        </a>
                                    </div>
                                </div>
                                <h4 className="title"><a >New: Freehand Templates, built for the whole team</a></h4>
                                <div className="post-meta-wrapper">
                                    <div className="post-meta">
                                        <div className="post-author-avatar border-rounded">
                                            <img src="assets/images/post-images/author/author-image-1.png" alt="Author Images" />
                                        </div>
                                        <div className="content">
                                            <h6 className="post-author-name">
                                                <a className="hover-flip-item-wrapper" >
                        <span className="hover-flip-item">
                          <span data-text="Mohima Afrin">Mohima Afrin</span>
                        </span>
                                                </a>
                                            </h6>
                                            <ul className="post-meta-list">
                                                <li>Feb 17, 2019</li>
                                                <li>3 min read</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <ul className="social-share-transparent justify-content-end">
                                        <li><a ><i className="fab fa-facebook-f" /></a></li>
                                        <li><a ><i className="fab fa-instagram" /></a></li>
                                        <li><a ><i className="fab fa-twitter" /></a></li>
                                        <li><a ><i className="fas fa-link" /></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {/* End Post List  */}
                        {/* Start Post List  */}
                        <div className="content-block post-list-view axil-control mt--30">
                            <div className="post-thumbnail">
                                <a >
                                    <img src="assets/images/post-images/post-list-08.jpg" alt="Post Images" />
                                </a>
                            </div>
                            <div className="post-content">
                                <div className="post-cat">
                                    <div className="post-cat-list">
                                        <a className="hover-flip-item-wrapper" >
                    <span className="hover-flip-item">
                      <span data-text="LIFESTYLE">LIFESTYLE</span>
                    </span>
                                        </a>
                                    </div>
                                </div>
                                <h4 className="title"><a >As a lifestyle you always being the focal point is innately unhealthy.</a></h4>
                                <div className="post-meta-wrapper">
                                    <div className="post-meta">
                                        <div className="post-author-avatar border-rounded">
                                            <img src="assets/images/post-images/author/author-image-2.png" alt="Author Images" />
                                        </div>
                                        <div className="content">
                                            <h6 className="post-author-name">
                                                <a className="hover-flip-item-wrapper" >
                        <span className="hover-flip-item">
                          <span data-text="Irin Pervin">Irin Pervin</span>
                        </span>
                                                </a>
                                            </h6>
                                            <ul className="post-meta-list">
                                                <li>Feb 17, 2019</li>
                                                <li>3 min read</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <ul className="social-share-transparent justify-content-end">
                                        <li><a ><i className="fab fa-facebook-f" /></a></li>
                                        <li><a ><i className="fab fa-instagram" /></a></li>
                                        <li><a ><i className="fab fa-twitter" /></a></li>
                                        <li><a ><i className="fas fa-link" /></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {/* End Post List  */}
                        {/* Start Post List  */}
                        <div className="content-block post-list-view axil-control mt--30">
                            <div className="post-thumbnail">
                                <a >
                                    <img src="assets/images/post-images/post-list-09.jpg" alt="Post Images" />
                                </a>
                            </div>
                            <div className="post-content">
                                <div className="post-cat">
                                    <div className="post-cat-list">
                                        <a className="hover-flip-item-wrapper" >
                    <span className="hover-flip-item">
                      <span data-text="FOOD">FOOD</span>
                    </span>
                                        </a>
                                    </div>
                                </div>
                                <h4 className="title"><a >I saw few die of hunger; of eating, a hundred thousand.</a></h4>
                                <div className="post-meta-wrapper">
                                    <div className="post-meta">
                                        <div className="post-author-avatar border-rounded">
                                            <img src="assets/images/post-images/author/author-image-2.png" alt="Author Images" />
                                        </div>
                                        <div className="content">
                                            <h6 className="post-author-name">
                                                <a className="hover-flip-item-wrapper" >
                        <span className="hover-flip-item">
                          <span data-text="Rafiq Islam">Rafiq Islam</span>
                        </span>
                                                </a>
                                            </h6>
                                            <ul className="post-meta-list">
                                                <li>Feb 17, 2019</li>
                                                <li>3 min read</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <ul className="social-share-transparent justify-content-end">
                                        <li><a ><i className="fab fa-facebook-f" /></a></li>
                                        <li><a ><i className="fab fa-instagram" /></a></li>
                                        <li><a ><i className="fab fa-twitter" /></a></li>
                                        <li><a ><i className="fas fa-link" /></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        {/* End Post List  */}
                    </div>
                    <div className="col-lg-4 col-xl-4 mt_md--40 mt_sm--40">
                        {/* Start Sidebar Area  */}
                        <div className="sidebar-inner">
                            {/* Start Single Widget  */}
                            <div className="axil-single-widget widget widget_categories mb--30">
                                <ul>
                                    <li className="cat-item">
                                        <a  className="inner">
                                            <div className="thumbnail">
                                                <img src="assets/images/post-images/category-image-01.jpg" alt="" />
                                            </div>
                                            <div className="content">
                                                <h5 className="title">Tech</h5>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="cat-item">
                                        <a  className="inner">
                                            <div className="thumbnail">
                                                <img src="assets/images/post-images/category-image-02.jpg" alt="" />
                                            </div>
                                            <div className="content">
                                                <h5 className="title">Style</h5>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="cat-item">
                                        <a  className="inner">
                                            <div className="thumbnail">
                                                <img src="assets/images/post-images/category-image-03.jpg" alt="" />
                                            </div>
                                            <div className="content">
                                                <h5 className="title">Travel</h5>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="cat-item">
                                        <a  className="inner">
                                            <div className="thumbnail">
                                                <img src="assets/images/post-images/category-image-04.jpg" alt="" />
                                            </div>
                                            <div className="content">
                                                <h5 className="title">Food</h5>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            {/* End Single Widget  */}
                            {/* Start Single Widget  */}
                            <div className="axil-single-widget widget widget_search mb--30">
                                <h5 className="widget-title">Search</h5>
                                <form action="#">
                                    <div className="axil-search form-group">
                                        <button type="submit" className="search-button"><i className="fal fa-search" /></button>
                                        <input type="text" className="form-control" placeholder="Search" />
                                    </div>
                                </form>
                            </div>
                            {/* End Single Widget  */}
                            {/* Start Single Widget  */}
                            <div className="axil-single-widget widget widget_postlist mb--30">
                                <h5 className="widget-title">Popular on Blogar</h5>
                                {/* Start Post List  */}
                                <div className="post-medium-block">
                                    {/* Start Single Post  */}
                                    <div className="content-block post-medium mb--20">
                                        <div className="post-thumbnail">
                                            <a >
                                                <img src="assets/images/small-images/blog-sm-01.jpg" alt="Post Images" />
                                            </a>
                                        </div>
                                        <div className="post-content">
                                            <h6 className="title"><a >The underrated design book that transformed the way I
                                                work </a></h6>
                                            <div className="post-meta">
                                                <ul className="post-meta-list">
                                                    <li>Feb 17, 2019</li>
                                                    <li>300k Views</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    {/* End Single Post  */}
                                    {/* Start Single Post  */}
                                    <div className="content-block post-medium mb--20">
                                        <div className="post-thumbnail">
                                            <a >
                                                <img src="assets/images/small-images/blog-sm-02.jpg" alt="Post Images" />
                                            </a>
                                        </div>
                                        <div className="post-content">
                                            <h6 className="title"><a >Here’s what you should (and shouldn’t) do when</a>
                                            </h6>
                                            <div className="post-meta">
                                                <ul className="post-meta-list">
                                                    <li>Feb 17, 2019</li>
                                                    <li>300k Views</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    {/* End Single Post  */}
                                    {/* Start Single Post  */}
                                    <div className="content-block post-medium mb--20">
                                        <div className="post-thumbnail">
                                            <a >
                                                <img src="assets/images/small-images/blog-sm-03.jpg" alt="Post Images" />
                                            </a>
                                        </div>
                                        <div className="post-content">
                                            <h6 className="title"><a >How a developer and designer duo at Deutsche Bank keep
                                                remote</a></h6>
                                            <div className="post-meta">
                                                <ul className="post-meta-list">
                                                    <li>Feb 17, 2019</li>
                                                    <li>300k Views</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    {/* End Single Post  */}
                                </div>
                                {/* End Post List  */}
                            </div>
                            {/* End Single Widget  */}
                            {/* Start Single Widget  */}
                            <div className="axil-single-widget widget widget_social mb--30">
                                <h5 className="widget-title">Stay In Touch</h5>
                                {/* Start Post List  */}
                                <ul className="social-icon md-size justify-content-center">
                                    <li><a ><i className="fab fa-facebook-f" /></a></li>
                                    <li><a ><i className="fab fa-instagram" /></a></li>
                                    <li><a ><i className="fab fa-twitter" /></a></li>
                                    <li><a ><i className="fab fa-slack" /></a></li>
                                    <li><a ><i className="fab fa-linkedin-in" /></a></li>
                                </ul>
                                {/* End Post List  */}
                            </div>
                            {/* End Single Widget  */}
                            {/* Start Single Widget  */}
                            <div className="axil-single-widget widget widget_instagram mb--30">
                                <h5 className="widget-title">Instagram</h5>
                                {/* Start Post List  */}
                                <ul className="instagram-post-list-wrapper">
                                    <li className="instagram-post-list">
                                        <a >
                                            <img src="assets/images/small-images/instagram-01.jpg" alt="Instagram Images" />
                                        </a>
                                    </li>
                                    <li className="instagram-post-list">
                                        <a >
                                            <img src="assets/images/small-images/instagram-02.jpg" alt="Instagram Images" />
                                        </a>
                                    </li>
                                    <li className="instagram-post-list">
                                        <a >
                                            <img src="assets/images/small-images/instagram-03.jpg" alt="Instagram Images" />
                                        </a>
                                    </li>
                                    <li className="instagram-post-list">
                                        <a >
                                            <img src="assets/images/small-images/instagram-04.jpg" alt="Instagram Images" />
                                        </a>
                                    </li>
                                    <li className="instagram-post-list">
                                        <a >
                                            <img src="assets/images/small-images/instagram-05.jpg" alt="Instagram Images" />
                                        </a>
                                    </li>
                                    <li className="instagram-post-list">
                                        <a >
                                            <img src="assets/images/small-images/instagram-06.jpg" alt="Instagram Images" />
                                        </a>
                                    </li>
                                </ul>
                                {/* End Post List  */}
                            </div>
                            {/* End Single Widget  */}
                        </div>
                        {/* End Sidebar Area  */}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PostListWrapper;
