<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/user', [App\Http\Controllers\UserController::class, 'index']);
Route::get('/configuration', [App\Http\Controllers\ConfigurationController::class, 'index']);
Route::get('/status', [App\Http\Controllers\StatusController::class, 'index']);
Route::get('/status/create', [App\Http\Controllers\StatusController::class, 'create']);


// Bao Ngoc
Route::prefix('baongoc')->group(function ()
{
	// index
	Route::get('/user', [App\Http\Controllers\baongoc\UserController::class, 'index']);
	Route::get('/shop', [App\Http\Controllers\baongoc\ShopBaoNgocController::class, 'index']);
	Route::get('/customer', [App\Http\Controllers\baongoc\CustomerController::class, 'index']);
	Route::get('/product', [App\Http\Controllers\baongoc\ProductController::class, 'index']);
	Route::get('/category', [App\Http\Controllers\baongoc\CategoryController::class, 'index']);
	Route::get('/blogs', [App\Http\Controllers\baongoc\BlogsController::class, 'index']);
	Route::get('/order', [App\Http\Controllers\baongoc\OrderController::class, 'index']);
	Route::get('/api', [App\Http\Controllers\baongoc\ApiBaoNgocController::class, 'index']);
	Route::get('/customfield', [App\Http\Controllers\baongoc\CustomFieldController::class, 'index']);
	Route::get('/customfieldcategory', [App\Http\Controllers\baongoc\CustomFieldCategoryController::class, 'index']);


	// Create
	Route::get('/shop/create', [App\Http\Controllers\baongoc\ShopBaoNgocController::class, 'create']);






});
// MER

