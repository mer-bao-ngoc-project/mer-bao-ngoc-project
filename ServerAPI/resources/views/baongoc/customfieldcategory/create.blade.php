@extends('layouts.admin')

@section('css')
	<link href="{{ asset('assets/libs/dropzone/min/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
	<!-- Begin page -->
	<div id="layout-wrapper">

	@include('layouts.header')

	@include('layouts.leftisdebar')

	<!-- ============================================================== -->
		<!-- Start right Content here -->
		<!-- ============================================================== -->
		<div class="main-content">

			<div class="page-content">
				<div class="container-fluid">

					<!-- start page title -->
					<div class="row">
						<div class="col-12">
							<div class="page-title-box d-sm-flex align-items-center justify-content-between">
								<h4 class="mb-sm-0 font-size-18">Shop</h4>

								<div class="page-title-right">
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">BaoNgoc</a></li>
										<li class="breadcrumb-item active">Create New Custom Field</li>
									</ol>
								</div>

							</div>
						</div>
					</div>
					<!-- end page title -->

					<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title mb-4">Create New Custom Field</h4>
									<form method="POST" action="">
										@csrf
										<div class="row mb-4">
											<label for="projectname" class="col-form-label col-lg-2">Custom Field</label>
											<div class="col-lg-10">
												<input id="custom_field_name" name="custom_field_name" type="text" class="form-control" placeholder="Enter Custom Field...">
											</div>
										</div>
										<div class="row mb-4">
											<label for="projectdesc" class="col-form-label col-lg-2">Project Description</label>
											<div class="col-lg-10">
												<textarea class="form-control" id="custom_field_description"  name="custom_field_description" rows="3" placeholder="Enter Custom Field Description..."></textarea>
											</div>
										</div>

										<div class="row mb-4">
											<label for="projectbudget" class="col-form-label col-lg-2">Budget</label>
											<div class="col-lg-10">
												<input id="projectbudget" name="projectbudget" type="text" placeholder="Enter Project Budget..." class="form-control">
											</div>
										</div>
									</form>
									<div class="row mb-4">
										<label class="col-form-label col-lg-2">Attached Files</label>
										<div class="col-lg-10">
											<form action="#" method="post" class="dropzone">
												<div class="fallback">
													<input name="file" type="file" multiple />
												</div>

												<div class="dz-message needsclick">
													<div class="mb-3">
														<i class="display-4 text-muted bx bxs-cloud-upload"></i>
													</div>

													<h4>Drop files here or click to upload.</h4>
												</div>
											</form>
										</div>
									</div>
									<div class="row justify-content-end">
										<div class="col-lg-10">
											<button type="submit" class="btn btn-primary">Create Project</button>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
					<!-- end row -->

				</div> <!-- container-fluid -->
			</div>
			<!-- End Page-content -->


			@include('layouts.fotter')
		</div>
		<!-- ============================================================== -->
		<!-- end right Content here -->
		<!-- ============================================================== -->
	</div>
	<!-- END layout-wrapper -->

	<!-- Right bar overlay-->
	<div class="rightbar-overlay"></div>



@endsection

@section('js')
<!-- bootstrap datepicker -->
<script src="{{ asset('assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<!-- dropzone plugin -->
<script src="{{ asset('assets/libs/dropzone/min/dropzone.min.js') }}"></script>
@endsection



