@extends('layouts.admin')

@section('content')
	<!-- Begin page -->
	<div id="layout-wrapper">

	@include('layouts.header')

	@include('layouts.leftisdebar')

		<!-- ============================================================== -->
		<!-- Start right Content here -->
		<!-- ============================================================== -->
		<div class="main-content">

			<div class="page-content">
				<div class="container-fluid">

					<!-- start page title -->
					<div class="row">
						<div class="col-12">
							<div class="page-title-box d-sm-flex align-items-center justify-content-between">
								<h4 class="mb-sm-0 font-size-18">Shop</h4>

								<div class="page-title-right">
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">BaoNgoc</a></li>
										<li class="breadcrumb-item active">Custom Field Category</li>
									</ol>
								</div>

							</div>
						</div>
					</div>
					<!-- end page title -->

					<div class="row">
						<div class="col-lg-12">
							<div class="">
								<div class="table-responsive">
									<table class="table project-list-table table-nowrap align-middle table-borderless">
										<thead>
										<tr>
											<th scope="col" style="width: 100px">#</th>
											<th scope="col">Projects</th>
											<th scope="col">Due Date</th>
											<th scope="col">Status</th>
											<th scope="col">Action</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td><img src="{{ asset('assets/images/companies/img-1.png') }}" alt="" class="avatar-sm"></td>
											<td>
												<h5 class="text-truncate font-size-14"><a href="#" class="text-dark">New admin Design</a></h5>
												<p class="text-muted mb-0">It will be as simple as Occidental</p>
											</td>
											<td>15 Oct, 19</td>
											<td><span class="badge bg-success">Completed</span></td>
											<td>
												<div class="dropdown">
													<a href="#" class="dropdown-toggle card-drop" data-bs-toggle="dropdown" aria-expanded="false">
														<i class="mdi mdi-dots-horizontal font-size-18"></i>
													</a>
													<ul class="dropdown-menu dropdown-menu-end">
														<li><a href="#" class="dropdown-item"><i class="mdi mdi-pencil font-size-16 text-success me-1"></i> Edit</a></li>
														<li><a href="#" class="dropdown-item"><i class="mdi mdi-trash-can font-size-16 text-danger me-1"></i> Delete</a></li>
													</ul>
												</div>
											</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- end row -->

				</div> <!-- container-fluid -->
			</div>
			<!-- End Page-content -->

		   @include('layouts.fotter')
		</div>
		<!-- ============================================================== -->
		<!-- end right Content here -->
		<!-- ============================================================== -->
	</div>
	<!-- END layout-wrapper -->

	<!-- Right bar overlay-->
	<div class="rightbar-overlay"></div>



@endsection

@section('js')
	<!-- Required datatable js -->
	<script src="{{ asset('assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<!-- Buttons examples -->
	<script src="{{ asset('assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/libs/jszip/jszip.min.js') }}"></script>
	<script src="{{ asset('assets/libs/pdfmake/build/pdfmake.min.js') }}"></script>
	<script src="{{ asset('assets/libs/pdfmake/build/vfs_fonts.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-buttons/js/buttons.colVis.min.js') }}"></script>

{{--	<!-- Responsive examples -->--}}
{{--	<script src="{{ asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>--}}
{{--	<script src="{{ asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>--}}

{{--	<!-- Datatable init js -->--}}
{{--	<script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>--}}
{{--	<script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>--}}
{{--	<script src="{{ asset('assets/js/pages/tasklist.init.js') }}"></script>--}}
@endsection

@section('css')

@endsection

