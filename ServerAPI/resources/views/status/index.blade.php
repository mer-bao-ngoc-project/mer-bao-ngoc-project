@extends('layouts.admin')

@section('content')
	<!-- Begin page -->
	<div id="layout-wrapper">

	@include('layouts.header')

	@include('layouts.leftisdebar')

		<!-- ============================================================== -->
		<!-- Start right Content here -->
		<!-- ============================================================== -->
		<div class="main-content">

			<div class="page-content">
				<div class="container-fluid">

					<!-- start page title -->
					<div class="row">
						<div class="col-12">
							<div class="page-title-box d-sm-flex align-items-center justify-content-between">
								<h4 class="mb-sm-0 font-size-18">Status</h4>

								<div class="page-title-right">
									<ol class="breadcrumb m-0">
										<li class="breadcrumb-item"><a href="#">BaoNgoc</a></li>
										<li class="breadcrumb-item active">Status</li>
									</ol>
								</div>

							</div>
						</div>
					</div>
					<!-- end page title -->

					<div class="row">
						<div class="col-12">
							<div class="card">
								<div class="card-body">
									<div class="row mb-2">
										<div class="col-sm-4">

										</div>
										<div class="col-sm-8">
											<div class="text-sm-end">
												<a href="{{ url('status/create') }}" type="button" class="btn btn-success btn-rounded waves-effect waves-light mb-2 me-2"><i class="mdi mdi-plus me-1"></i> Add New Order</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-3">
							<div class="card border border-primary">
								<div class="card-header bg-transparent border-primary">
									<h5 class="my-0 text-primary"><i class="mdi mdi-bullseye-arrow me-3"></i>Primary outline Card</h5>
								</div>
								<div class="card-body">
									<h5 class="card-title mt-0">card title</h5>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									<a href="#" class="btn btn-primary waves-effect waves-light">Edit</a>
								</div>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="card border border-danger">
								<div class="card-header bg-transparent border-danger">
									<h5 class="my-0 text-danger"><i class="mdi mdi-block-helper me-3"></i>Danger outline Card</h5>
								</div>
								<div class="card-body">
									<h5 class="card-title mt-0">card title</h5>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									<a href="#" class="btn btn-primary waves-effect waves-light">Edit</a>
								</div>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="card border border-primary">
								<div class="card-header bg-transparent border-primary">
									<h5 class="my-0 text-primary"><i class="mdi mdi-bullseye-arrow me-3"></i>Primary outline Card</h5>
								</div>
								<div class="card-body">
									<h5 class="card-title mt-0">card title</h5>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									<a href="#" class="btn btn-primary waves-effect waves-light">Edit</a>
								</div>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="card border border-danger">
								<div class="card-header bg-transparent border-danger">
									<h5 class="my-0 text-danger"><i class="mdi mdi-block-helper me-3"></i>Danger outline Card</h5>
								</div>
								<div class="card-body">
									<h5 class="card-title mt-0">card title</h5>
									<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
									<a href="#" class="btn btn-primary waves-effect waves-light">Edit</a>
								</div>
							</div>
						</div>
					</div>
					<!-- end row -->



				</div> <!-- container-fluid -->
			</div>
			<!-- End Page-content -->

			@include('layouts.fotter')
		</div>
		<!-- ============================================================== -->
		<!-- end right Content here -->
		<!-- ============================================================== -->
	</div>
	<!-- END layout-wrapper -->

	<!-- Right bar overlay-->
	<div class="rightbar-overlay"></div>



@endsection

@section('js')
	<!-- Required datatable js -->
	<script src="{{ asset('assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<!-- Edits examples -->
	<script src="{{ asset('assets/libs/datatables.net-Edits/js/dataTables.Edits.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-Edits-bs4/js/Edits.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/libs/jszip/jszip.min.js') }}"></script>
	<script src="{{ asset('assets/libs/pdfmake/build/pdfmake.min.js') }}"></script>
	<script src="{{ asset('assets/libs/pdfmake/build/vfs_fonts.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-Edits/js/Edits.html5.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-Edits/js/Edits.print.min.js') }}"></script>
	<script src="{{ asset('assets/libs/datatables.net-Edits/js/Edits.colVis.min.js') }}"></script>

	{{--	<!-- Responsive examples -->--}}
	{{--	<script src="{{ asset('assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>--}}
	{{--	<script src="{{ asset('assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>--}}

	{{--	<!-- Datatable init js -->--}}
	{{--	<script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>--}}
	{{--	<script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>--}}
	{{--	<script src="{{ asset('assets/js/pages/tasklist.init.js') }}"></script>--}}
@endsection

@section('css')

@endsection

