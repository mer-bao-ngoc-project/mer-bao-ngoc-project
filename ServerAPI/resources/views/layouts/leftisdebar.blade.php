<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">
	<div class="h-100">
		<!--- Sidemenu -->
		<div id="sidebar-menu">
			<!-- Left Menu Start -->
			<ul class="metismenu list-unstyled" id="side-menu">
				<li class="menu-title" key="t-menu">API</li>
				<li>
					<a href="#" class="waves-effect">
						<i class="bx bx-home-circle"></i>
						<span >Bao Ngoc</span>
					</a>
					<ul class="sub-menu" aria-expanded="false">
						<li><a href="{{ url('baongoc/customer') }}" >Customer</a></li>
						<li><a href="{{ url('baongoc/user') }}" >User</a></li>
						<li><a href="{{ url('baongoc/product') }}" >Product</a></li>
						<li><a href="{{ url('baongoc/category') }}" >Category</a></li>
						<li><a href="{{ url('baongoc/customfieldcategory') }}" >Custom Field Category</a></li>
						<li><a href="{{ url('baongoc/customfield') }}" >Custom Field</a></li>
						<li><a href="{{ url('baongoc/shop') }}" >Shop</a></li>
						<li><a href="{{ url('baongoc/order') }}" >Order</a></li>
						<li><a href="{{ url('baongoc/blogs') }}" >Blogs</a></li>
						<li><a href="{{ url('baongoc/api') }}" >API</a></li>
					</ul>
				</li>
				<li>
					<a href="#" class="waves-effect">
						<i class="bx bx-home-circle"></i>
						<span >MER</span>
					</a>
					<ul class="sub-menu" aria-expanded="false">
						<li><a href="#" >Customer</a></li>
						<li><a href="#" >User</a></li>
						<li><a href="#" >Products</a></li>
						<li><a href="#" >Category</a></li>
						<li><a href="#" >Shop</a></li>
						<li><a href="#" >Order</a></li>
						<li><a href="#" >Blogs</a></li>
					</ul>
				</li>
				<li>
					<a href="#" class="waves-effect">
						<i class="bx bx-home-circle"></i>
						<span >Laptop Bao Ngoc</span>
					</a>
					<ul class="sub-menu" aria-expanded="false">
						<li><a href="#" >Customer</a></li>
						<li><a href="#" >User</a></li>
						<li><a href="#" >Products</a></li>
						<li><a href="#" >Category</a></li>
						<li><a href="#" >Shop</a></li>
						<li><a href="#" >Order</a></li>
						<li><a href="#" >Blogs</a></li>
					</ul>
				</li>
				<li>
					<a href="#" class="waves-effect">
						<i class="bx bx-home-circle"></i>
						<span >Router - Wifi Bao Ngoc</span>
					</a>
					<ul class="sub-menu" aria-expanded="false">
						<li><a href="#" >Customer</a></li>
						<li><a href="#" >User</a></li>
						<li><a href="#" >Products</a></li>
						<li><a href="#" >Category</a></li>
						<li><a href="#" >Shop</a></li>
						<li><a href="#" >Order</a></li>
						<li><a href="#" >Blogs</a></li>
					</ul>
				</li>
				<li class="menu-title" key="t-menu">System</li>
				<li>
					<a href="{{ url('/user') }}" class="waves-effect">
						<i class="bx bx-chat"></i>
						<span key="t-chat">User</span>
					</a>
				</li>
				<li>
					<a href="{{ url('/configuration') }}" class="waves-effect">
						<i class="bx bx-chat"></i>
						<span key="t-chat">Configuration</span>
					</a>
				</li>
				<li>
					<a href="{{ url('/status') }}" class="waves-effect">
						<i class="bx bx-chat"></i>
						<span key="t-chat">Status</span>
					</a>
				</li>

			</ul>
		</div>
		<!-- Sidebar -->
	</div>
</div>
<!-- Left Sidebar End -->
